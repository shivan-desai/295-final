import './App.css';
import DragAndDrop from './DragAndDrop';
import PickImages from './PickImages';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";


function App() {
  return (
    <div className="App">
      <header className="App-header">
      <Router>
        <Switch>
          <Route exact path="/">
            <DragAndDrop/>
          </Route>
          <Route exact path="/images">
            <PickImages />
          </Route>
        </Switch>
    </Router>
      </header>
    </div>
  );
}

export default App;
