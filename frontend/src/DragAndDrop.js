import React, { useRef, useState, useEffect } from 'react';
import axios from 'axios';
import './DragAndDrop.css';
// import './Dropzone.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUpload, faSpinner } from '@fortawesome/free-solid-svg-icons';
import {withRouter} from 'react-router-dom';
import{
    Grid,
    makeStyles,
    Typography,
    Button,
    Card
} from '@material-ui/core';
import {
    Close,
    CloudUpload
} from '@material-ui/icons';

import {properties} from "./properties";

const BACKEND_URL = properties.backendhost;
const UPLOAD_VIDEO_ENDPOINT = '/uploadVideo';
const GET_FINAL_RESULT_ENDPOINT = '/getResults';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        width: "80%"
    },
    dropContainer: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        margin: 0,
        padding: "2%",
        width: "80%",
        marginLeft: "10%",
        // height: "200px",
        border: "1px solid #B99858",
        borderRadius: "20px",
        '&:hover': {
            cursor: "pointer"
        }
    },
    cloudIcon: {
        width: "60%",
        marginLeft: "20%",
        margin: "10%",
        // height: "60%",
        fontSize: "8em",
        color: "rgba(150,150,150,0.2)"
    },
    fileUploadBtn: {
        color: "white",
        textTransform: "uppercase",
        outline: "none",
        backgroundColor: "#4aa1f3",
        fontWeight: "bold",
        padding: "8px 15px",
        marginBottom: "5px",
    },
    fileInput: {
        display: "none"
    },
    fileStatusBar: {
        width: "100%",
        verticalAlign: "top",
        marginTop: "10px",
        marginBottom: "20px",
        position: "relative",
        lineHeight: "50px",
        height: "50px",
    },
    fileType: {
        display: "inline-block!important",
        position: "absolute",
        fontSize: "12px",
        fontWeight: "700",
        lineHeight: "13px",
        marginTop: "25px",
        padding: "0 4px",
        borderRadius: "2px",
        boxShadow: "1px 1px 2px #abc",
        color: "#fff",
        background: "#0080c8",
        textTransform: "uppercase",
    },
    fileSize: {
        display:"inline-block",
        verticalAlign:"top",
        color:"#30693D",
        marginLeft:"10px",
        marginRight:"5px",
        color: "#444242",
        fontWeight: "700",
        fontSize: "14px"
    },
    fileRemove: {
        position: "absolute",
        top: "20px",
        right: "10px",
        lineHeight: "15px",
        cursor: "pointer",
        color: "red",
        marginRight: "-10px",
    },
    uploadModal: {
        zIndex: "999",
        display: "none",
        overflow: "hidden",
    },
    close: {
        position: "absolute",
        top: "15px",
        right: "35px",
        color: "#f1f1f1",
        fontSize: "40px",
        fontWeight: "bold",
        transition: "0.3s",
    },
    progress: {
        width: "90%",
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)",
        backgroundColor:" #efefef",
        height: "20px",
        borderRadius: "5px",
    },
    progressBar: {
        position: "absolute",
        backgroundColor: "#4aa1f3",
        height: "20px",
        borderRadius: "5px",
        textAlign: "center",
        color: "white",
        fontWeight: "bold",
    },
    title: {
        fontWeight: "bold",
        fontSize: "1.5em",
        width: "50%",
        marginLeft: "25%"
    },
    uploadVideoButton: {
        backgroundColor: "#B99858",
        '&:hover': {
            backgroundColor: "#B99F58"
        }
    },
    photoCard: {
        // height: "50%"
        border: "3px solid black",
        '&:hover': {
            cursor: "pointer",
            border: "3px solid green"
        }
    },
    photosRoot: {
        width: "90%",
        marginLeft: "5%"
    },
    photo: {
        width: "70%"
    },
    spinner: {

    }
}));

const Dropzone = () => {
    const classes = useStyles();
    const fileInputRef = useRef();
    const modalImageRef = useRef();
    const modalRef = useRef();
    const progressRef = useRef();
    const uploadRef = useRef();
    const uploadModalRef = useRef();
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [validFiles, setValidFiles] = useState([]);
    const [unsupportedFiles, setUnsupportedFiles] = useState([]);
    const [errorMessage, setErrorMessage] = useState('');
    const [step, setStep] = useState(0);
    const [photos, setPhotos] = useState({files: []});
    const [selectedPhotos, setSelectedPhotos] = useState([]);
    const [isDeepFake, setIsDeepFake] = useState(false);

    useEffect(() => {
        let filteredArr = selectedFiles.reduce((acc, current) => {
            const x = acc.find(item => item.name === current.name);
            if (!x) {
              return acc.concat([current]);
            } else {
              return acc;
            }
        }, []);
        setValidFiles([...filteredArr]);
        
    }, [selectedFiles]);

    const preventDefault = (e) => {
        e.preventDefault();
        // e.stopPropagation();
    }

    const dragOver = (e) => {
        preventDefault(e);
    }

    const dragEnter = (e) => {
        preventDefault(e);
    }

    const dragLeave = (e) => {
        preventDefault(e);
    }

    const fileDrop = (e) => {
        preventDefault(e);
        const files = e.dataTransfer.files;
        if (files.length) {
            handleFiles(files);
        }
    }

    const filesSelected = () => {
        if (fileInputRef.current.files.length) {
            handleFiles(fileInputRef.current.files);
        }
    }

    const fileInputClicked = () => {
        fileInputRef.current.click();
    }

    const handleFiles = (files) => {
        for(let i = 0; i < files.length; i++) {
            if (validateFile(files[i])) {
                setSelectedFiles(prevArray => [...prevArray, files[i]]);
            } else {
                files[i]['invalid'] = true;
                setSelectedFiles(prevArray => [...prevArray, files[i]]);
                setErrorMessage('File type not permitted');
                setUnsupportedFiles(prevArray => [...prevArray, files[i]]);
            }
        }
    }

    const validateFile = (file) => {
        const validTypes = ['video/mp4'];
        if (validTypes.indexOf(file.type) === -1) {
            return false;
        }

        return true;
    }

    const fileSize = (size) => {
        if (size === 0) {
          return '0 Bytes';
        }
        const k = 1024;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        const i = Math.floor(Math.log(size) / Math.log(k));
        return parseFloat((size / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }

    const fileType = (fileName) => {
        return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length) || fileName;
    }

    const removeFile = (name) => {
        const index = validFiles.findIndex(e => e.name === name);
        const index2 = selectedFiles.findIndex(e => e.name === name);
        const index3 = unsupportedFiles.findIndex(e => e.name === name);
        validFiles.splice(index, 1);
        selectedFiles.splice(index2, 1);
        setValidFiles([...validFiles]);
        setSelectedFiles([...selectedFiles]);
        if (index3 !== -1) {
            unsupportedFiles.splice(index3, 1);
            setUnsupportedFiles([...unsupportedFiles]);
        }
    }

    const uploadFiles = () => {
        setStep(-1);
        const formData = new FormData();
        formData.append('file', validFiles[0]);
        var that = this;
        axios.post(BACKEND_URL + UPLOAD_VIDEO_ENDPOINT, formData//}, {
            // onUploadProgress: (progressEvent) => {
            //     const uploadPercentage = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
            //     progressRef.current.innerHTML = `${uploadPercentage}%`;
            //     progressRef.current.style.width = `${uploadPercentage}%`;

            //     if (uploadPercentage === 100) {
            //         uploadRef.current.innerHTML = 'File(s) Uploaded';
            //         validFiles.length = 0;
            //         setValidFiles([...validFiles]);
            //         setSelectedFiles([...validFiles]);
            //         setUnsupportedFiles([...validFiles]);
            //     }
            // },
        // }
        )
        .then((res) => {
            console.log(res);
            setPhotos(res.data);
            setStep(1);
        })
        .catch(() => {
            uploadRef.current.innerHTML = `<span class="error">Error Uploading File(s)</span>`;
            progressRef.current.style.backgroundColor = 'red';
        })
    }

    const closeUploadModal = () => {
        uploadModalRef.current.style.display = 'none';
    }

    const handleSubmit = () => {
        setStep(-1);
        if(selectedPhotos.length === 0){
            alert("Please select atleast one photo");
            return;
        }
        axios.post(BACKEND_URL + GET_FINAL_RESULT_ENDPOINT, {
            selectedPhotos: selectedPhotos,
            videoName: photos.videoName
        })
        .then((res) => {
            console.log("final result",res);
            var resultArray = res.data[0].split(" ");
            var zeroCount = 0;
            var oneCount = 0;
            for(var i in resultArray){
                if(parseInt(resultArray[i]) === 1)
                    oneCount++;
                else
                    zeroCount++;
            }
            if(oneCount > zeroCount){
                setIsDeepFake(true);
            }
            else{
                setIsDeepFake(false);
            }
            setStep(2);
        })
        .catch((err) => {
            console.log("error", err);
        })
    }

    const handlePhotoClick = (photo) => {
        console.log("photo", photo);
        var selectedPhotosArray = selectedPhotos;
        selectedPhotos.push(photo);
        setSelectedPhotos(selectedPhotosArray);
    }

    return (
        <>
        {step === -1 && 
            <FontAwesomeIcon icon={faSpinner} className='spinner'/>
        }
        {step === 0 && 
        <React.Fragment>
            <Grid container direction={'column'} className={classes.root}>
                <Grid item><Typography className={classes.title}>Upload your file</Typography></Grid>
                <Grid item>
                    <Grid container
                        direction={'column'}
                        className={classes.dropContainer}
                        onDragOver={dragOver}
                        onDragEnter={dragEnter}
                        onDragLeave={dragLeave}
                        onDrop={fileDrop}
                        onClick={fileInputClicked}  
                    >
                        <Grid item xs={12}>
                            {/* <CloudUpload fontSize={"2em"} className={classes.cloudIcon}/> */}
                            <FontAwesomeIcon icon={faUpload} className={classes.cloudIcon}/>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography>
                                Drag & Drop files here or click to select file(s)
                            </Typography>
                        </Grid>
                        <Grid item>
                            <input
                                ref={fileInputRef}
                                className={classes.fileInput}
                                type="file"
                                accept="video/mp4"
                                multiple
                                onChange={filesSelected}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item>
                    <Grid container direction={'column'}>
                    {
                        validFiles.map((data, i) => 
                            <Grid item className={classes.fileStatusBar} key={i}>
                                <div>
                                    <div className={classes.fileType}>{fileType(data.name)}</div>
                                    <span className={`file-name ${data.invalid ? 'file-error' : ''}`}>{data.name}</span>
                                    <span className={classes.fileSize}>({fileSize(data.size)})</span> {data.invalid && <span className='file-error-message'>({errorMessage})</span>}
                                </div>
                                <div className={classes.fileRemove} onClick={() => removeFile(data.name)}><Close/></div>
                            </Grid>
                        )
                    }
                    </Grid>
                </Grid>

                <Grid item>
                    <Button onClick={uploadFiles} className={classes.uploadVideoButton}>Upload Video</Button>
                </Grid>
            </Grid>

            <div className={classes.uploadModal} ref={uploadModalRef}>
                <div className={classes.close} onClick={(() => closeUploadModal())}>X</div>
                <div className="progress-container">
                    <span ref={uploadRef}></span>
                    <div className={classes.progress}>
                        <div className={classes.progressBar} ref={progressRef}></div>
                    </div>
                </div>
            </div>
        </React.Fragment>
        }
        {step === 1 &&
            <Grid container className={classes.photosRoot} spacing={2} direction="column">
                <Grid item>
                    <h3>Please click the image with the face that you want to track</h3>
                </Grid>
                <Grid item>
                    <Grid container spacing={2}>
                    {
                        photos.files.map((photo) => {
                            return(
                            <Grid item key={photo.fileName}>
                                <Card className={classes.photoCard} onClick={() => handlePhotoClick(photo)}>
                                    <img className={classes.photo} src={photo.src}/>
                                </Card>
                            </Grid>
                            );
                        })
                    }
                    </Grid>
                </Grid>
                <Grid item>
                    <Button onClick={handleSubmit}>Submit</Button>
                </Grid>
            </Grid>
        }
        {step === 2 && 
            <h1>Deepfake: {{isDeepFake} ? 'true' : 'false'}</h1>
        }
        </>
    );
}

export default withRouter(Dropzone);