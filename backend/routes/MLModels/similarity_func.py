# -*- coding: utf-8 -*-
"""
Created on Sat Mar 20 23:57:42 2021

@author: theor
"""

#%% Load imports
import cv2
#from skimage.measure import compare_ssim
from skimage.metrics import structural_similarity
# soon to be moved to skimage.metrics.structural_similarity

#%% Set up CV2 resources
sift = cv2.xfeatures2d.SIFT_create()

#%% Define similarity function OpenCV style
def similar( im1, im2 ):
    
    # Find key points and make description
    kp_1, desc_1 = sift.detectAndCompute(im1, None)
    kp_2, desc_2 = sift.detectAndCompute(im2, None)
    
    # Look for matches
    index_params = dict(algorithm=0, trees=5)
    search_params = dict()
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(desc_1, desc_2, k=2)
    
    # Find good points
    good_points = []
    ratio = 0.6
    for m, n in matches:
        if m.distance < ratio*n.distance:
            good_points.append(m)
            # print(len(good_points))
    # result = cv2.drawMatches(im1, kp_1, im2, kp_2, good_points, None)
    
    # Compare key points
    number_keypoints = 0
    if len(kp_1) <= len(kp_2):
        number_keypoints = len(kp_1)
    else:
        number_keypoints = len(kp_2)
    # print("Keypoints 1ST Image: " + str(len(kp_1)))
    # print("Keypoints 2ND Image: " + str(len(kp_2)))
    # Quantify similarity
    # print("GOOD Matches:", len(good_points))
    # print("How good it's the match: ", len(good_points) / number_keypoints * 100, "%")
    return len(good_points) / number_keypoints * 100

#%% SSIM similarity
def ssim_similarity( im1, im2 ):
    
    grayA = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
    grayB = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)
    
    (score, diff) = structural_similarity(grayA, grayB, full=True)
    diff = (diff * 255).astype("uint8")
    
    #print("SSIM: {}".format(score))
    return score

#%% SSIM similarity color
def ssim_similarity_color( im1, im2 ):
    
    (score, diff) = structural_similarity(im1, im2, full=True, multichannel = True)
    diff = (diff * 255).astype("uint8")
    
    #print("SSIM: {}".format(score))
    return score