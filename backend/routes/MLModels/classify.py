# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 20:02:06 2021

@author: theor
"""

#%% Load imports
import torch
import torch.nn as nn
from torchvision import transforms
import torchvision.datasets as dset
import pandas as pd
# from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from joblib import load
import sys

#%% Parse cmd line arguments
# ADD CODE HERE TO PARSE CMD LINE ARGS

#%% Define parameters
nc = 3
ndf = 8
ngpu = 0

#%% Class definition
class Discriminator(nn.Module):
    def __init__(self, ngpu):
        super(Discriminator, self).__init__()
        self.ngpu = ngpu
        self.main = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(nc, ndf, 3, 1, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            nn.MaxPool2d(kernel_size=(2, 2)),
            # state size. (ndf) x 32 x 32
            nn.Conv2d(ndf, ndf, 5, 1, 2, bias=False),
            nn.BatchNorm2d(ndf),
            nn.LeakyReLU(0.2, inplace=True),
            nn.MaxPool2d(kernel_size=(2, 2)),
            # state size. (ndf*2) x 16 x 16
            nn.Conv2d(ndf, ndf * 2, 5, 1, 2, bias=False),
            nn.BatchNorm2d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),
            nn.MaxPool2d(kernel_size=(2, 2)),
            # state size. (ndf*4) x 8 x 8
            nn.Conv2d(ndf * 2, ndf * 2, 5, 1, 2, bias=False),
            nn.BatchNorm2d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),
            nn.MaxPool2d(kernel_size=(2, 2)),
            # state size. (ndf*8) x 4 x 4
            # reshape batch_size * 16 * 4 * 4 -> batch_size * 256
        )

        self.lin = nn.Sequential(
            # dims = batch_size * 256
            nn.Linear(256,1),
            nn.Sigmoid()
            # final dims = batch_size * 1
        )

    def forward(self, X):
        X = self.main(X)
        X = torch.reshape(X, (-1, ndf * 2 * 4 * 4))
        X = self.lin(X)
        return X

#%% 
wt_list = ["routes/MLModels/POST_dis_weights.pth",
           "routes/MLModels/PRE_POST_dis_weights.pth",
           "routes/MLModels/styleGAN_state.pth"]

#%% Get predictions
def is_df(ip):
    model = Discriminator(ngpu)
    model.eval()
    res = []
    for pth in wt_list:
        model.load_state_dict(torch.load(pth, map_location=torch.device('cpu')))
        res.append(model(ip).mean().item())
    
    return res

#%% Load data
# CHANGE ROOT DIRECTORY - SHOULD MATCH CMD LINE ARGUMENT
dataset = dset.ImageFolder(root="frames_test",
                           transform=transforms.Compose([
                               transforms.Resize(64),
                               #transforms.CenterCrop(64),
                               transforms.ToTensor(),
                               transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                           ]))
# CHANGE BATCH SIZE TO MATCH # OF IMAGES
# CHANGE NUM WORKERS TO NUMBER OF AVAIL PROCESSORS
dataloader = torch.utils.data.DataLoader(dataset, batch_size=4,
                                         shuffle=False, num_workers=4)

#%% Pass through 3 discriminators
df = pd.DataFrame(columns=["post","prepost","style"])
for image, label in dataloader:
    pred = is_df(image)
    df=df.append({"post":pred[0],
                  "prepost":pred[1],
                  "style":1-pred[2]},
                 ignore_index=True)

#%% Load SVC
scaler = StandardScaler()
svm = load('routes/MLModels/svm_ensembling.joblib')

#%% Pass data through StandardScaler and SVC
normalized_data = scaler.fit_transform(df.to_numpy())
result = svm.predict(normalized_data)
print(result)
sys.stdout.flush()
# RETURN RESULTS




