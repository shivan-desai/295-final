# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 14:37:42 2021

@author: theor
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 01:06:15 2021

@author: theor
"""

#%% Load imports
import os
import cv2     # for capturing videos
from PIL import Image # for image data processing
import sys


#%% Load facial detection
faceCascade = cv2.CascadeClassifier('routes/MLModels/haarcascade_frontalface_default.xml') # Load pre-trained XML data
#secondary = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
side_length = 400 # set crop parameters for later

#%% Facial detection fn
option_scores = dict()
def process_image( cv2_img, prev_dict, name, number ):
    '''
    

    Parameters
    ----------
    cv2_img : CV2 image
        Image to be processed and saved
    name : original video name
        Name of video being processed
    number : Frame number
        Frame number within video

    Returns
    -------
    None.

    '''
    gray = cv2.cvtColor( cv2_img, cv2.COLOR_BGR2GRAY ) # Convert to grayscale
    faces = faceCascade.detectMultiScale(gray, 1.1, 1) # look for faces
    if not os.path.exists(f'initial/{name}'):
        os.makedirs(f'initial/{name}')
    option_num = 1
    
    # Convert to PIL image
    img = cv2.cvtColor(cv2_img, cv2.COLOR_BGR2RGB)
    pil_image = Image.fromarray(img)
    filename = f"initial/{name}/full_frame.jpg"
    pil_image.save(filename, "JPEG")
    for (x, y, w, h) in faces:
        # print(f"x = {x}, y = {y}, w = {w}, h = {h},")
        # im = Image.open(r"test_frames/aagfhgtpmv/frame_054.jpg")
        
        # Find center
        x_center = x + w//2
        y_center = y + h//2
        
        # Crop, (left, top, right, bottom) lines as coord
        cropped_image = pil_image.crop((x_center - side_length//2,
                                    y_center - side_length//2,
                                    x_center + side_length//2,
                                    y_center + side_length//2 ))
        filename = f"initial/{name}/face{option_num}.jpg"
        cropped_image.save(filename, "JPEG")
        option_num += 1


#%% Main fn loop
def Main(  ):
    # initialize parameters
    videoName = sys.argv[1]
    # videoName = 'uploads/mMuYVlaIVozCRRTw3tYVSaNn.mp4'
    #os.mkdir(f'frames/{videoFile}')

    cap = cv2.VideoCapture(videoName)   # capturing the video from the given path
    # set up faces dict and initial faces
    face_dict = dict()
    # process entire video
    print(cap.isOpened())
    sys.stdout.flush()
    while(cap.isOpened()):
        frame_number = '000'
        #filename = f"frames/{is_deepfake}/{videoFile}_frame_{pad + count_str}.jpg"
        ret, frame = cap.read()
        if (ret != True):
            # log error
            break
        # Process image
        process_image(frame, face_dict, videoName, frame_number)
        break
    cap.release()
    # print('done')
    # sys.stdout.flush()
    # compile and return list of saved images

Main()

#%% Return detected faces to user

# handle incoming http request
# take attached video, send to Main()
# get return from Main() and return images to frontend