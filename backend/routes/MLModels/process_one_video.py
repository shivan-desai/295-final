# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 01:06:15 2021

@author: theor
"""

#%% Load imports
import os
from os import walk
import cv2     # for capturing videos
from PIL import Image # for image data processing
import sys
import numpy as np
from similarity_func import ssim_similarity_color as similar

#COMMAND LINE ARGUMENTS: PATH TO VIDEO, DIRECTORY OF INITIAL FRAMES

#%% Load facial detection
faceCascade = cv2.CascadeClassifier('routes/MLModels/haarcascade_frontalface_default.xml') # Load pre-trained XML data
#secondary = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
side_length = 400 # set crop parameters for later

videoPath = sys.argv[1]
initialFramesDir = sys.argv[2]
#%% Storing video frames
if not os.path.exists('./frames_test/1'):
    os.makedirs('./frames_test/1')

#%% Define facial detection fn
option_scores = dict()
def process_image( cv2_img, prev_dict, name, number ):
    '''
    

    Parameters
    ----------
    cv2_img : CV2 image
        Image to be processed and saved
    label : is_deepfake
        Frame label on whether or not is deepfake; used to sort images into folders
    name : original video name
        Name of video being processed
    number : Frame number
        Frame number within video

    Returns
    -------
    None.

    '''
    results = []
    gray = cv2.cvtColor( cv2_img, cv2.COLOR_BGR2GRAY ) # Convert to grayscale
    faces = faceCascade.detectMultiScale(gray, 1.1, 1) # look for faces
    # Convert to PIL image
    img = cv2.cvtColor(cv2_img, cv2.COLOR_BGR2RGB)
    pil_image = Image.fromarray(img)
    for (x, y, w, h) in faces:        
        # Find center
        x_center = x + w//2
        y_center = y + h//2
        
        # Crop, (left, top, right, bottom) lines as coord
        cropped_image = pil_image.crop((x_center - side_length//2,
                                    y_center - side_length//2,
                                    x_center + side_length//2,
                                    y_center + side_length//2 ))
        results.append(cv2.cvtColor(np.array(cropped_image), cv2.COLOR_RGB2BGR))
        # filename = f"options/{name}_frame{number}_option{option_num}.jpg"
        # option_num += 1
        # cropped_image.save(filename, "JPEG")
    
    for k in prev_dict.keys(): # for each target face
        last_face = prev_dict[k][-1]
        best_face = last_face*0
        best_match = -1
        # iterate through results and find best match
        option_scores[number] = []
        for pic in results:
            # Convert PIL image into CV2 image
            s = similar(last_face, pic)
            option_scores[number].append(s)
            if s > best_match:
                best_match = s
                best_face = pic
        # Save
        prev_dict[k].append(best_face)
        color_converted = cv2.cvtColor(best_face, cv2.COLOR_BGR2RGB)
        im_pil = Image.fromarray(color_converted)
        filename = f"./frames_test/1/{name}_face{k}_frame{number}.jpg" ## CHANGE FILE DEST
        im_pil.save(filename, "JPEG")

#%%

# initialize parameters
count = 0

## VIDEO CAPTURE, CHANGE TO TAKE CMD LINE INPUT 1
cap = cv2.VideoCapture(videoPath)   # capturing the video from the given path
frameRate = cap.get(cv2.CAP_PROP_FPS) #get video frame rate
# set up faces dict and initial faces
face_dict = dict()
_, _, filenames_list = next(walk(initialFramesDir)) ## WALK OVER DIRECTORY OF INITIAL FRAMES
for name in filenames_list:
    key = int(name.split(".")[0]) ## BE CAREFUL OF NAMING CONVENTIONS, SHOULD BE CONVERTIBLE TO INT
    file_path = initialFramesDir + "/" + name ## CHANGE PATH TO DIRECTORY NAME/ + NAME
    value = cv2.imread( file_path )
    face_dict[key] = [value]
    ## DELETE IMAGE FROM FILE SYSTEM

# process entire video
while(cap.isOpened()):
    frameId = cap.get(1) #current frame number
    count_str = str(count)
    pad = '0'*(3 - len(count_str)) ## NAMING CONVENTION (IE # OF LEADING ZEROS) MAY NEED TO CHANGE
    frame_number = pad + count_str
    #filename = f"frames/{is_deepfake}/{videoFile}_frame_{pad + count_str}.jpg"
    ret, frame = cap.read()
    if not ret:
        break ## MAYBE RETURN ERROR MESSAGE
    # Process image
    videoFile = videoPath.split("/")[2].split(".")[0]
    if (frameId % (round(frameRate)/2) == 0): # Save every nth frame
        process_image(frame, face_dict, videoFile, frame_number)
    count += 1
cap.release()
# print('frames_test')
# sys.stdout.flush()
## RETURN OUTPUT DIRECTORY PATH IF DIFFERENT