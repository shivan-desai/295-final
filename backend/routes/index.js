var express = require('express');
var router = express.Router();
const { PythonShell } = require('python-shell');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart({uploadDir: './uploads'});
var zip = require('express-zip');
const path = require('path');
const fs = require('fs');
const AWS = require('aws-sdk');
const keys = require("../keys.js");


const PHOTO_FOLDER = `${__dirname}/../initial/uploads/`;

//setting the credentials
//The region should be the region of the bucket that you created
//Visit this if you have any confusion - https://docs.aws.amazon.com/general/latest/gr/rande.html
AWS.config.update({
  accessKeyId: keys.iam_access_id,
  secretAccessKey: keys.iam_secret,
  region: 'us-west-2',
});

//Creating a new instance of S3:
const s3= new AWS.S3();

function uploadFile(source,targetName){
  console.log('preparing to upload...');
  fs.readFile(source, function (err, filedata) {
    if (!err) {
      const putParams = {
          Bucket      : '275-deepfake-images',
          Key         : targetName,
          Body        : filedata
      };
      s3.putObject(putParams, function(err, data){
        if (err) {
          console.log('Could nor upload the file. Error :',err);
          // return res.send({success:false});
          return false;
        } 
        else{
          // fs.unlink(source);// Deleting the file from uploads folder(Optional).Do Whatever you prefer.
          console.log('Successfully uploaded the file', data);
          // return res.send({success:true});
          return true;
        }
      });
    }
    else{
      console.log({'err':err});
    }
  });
}

//The retrieveFile function
// function retrieveFile(filename,res){
//   const getParams = {
//     Bucket: 'sample-bucket-name',
//     Key: filename
//   };

//   s3.getObject(getParams, function(err, data) {
//     if (err){
//       return res.status(400).send({success:false,err:err});
//     }
//     else{
//       return res.send(data.Body);
//     }
//   });
// }

/* GET home page. */
router.get('/', function(req, res, next) {
  try{
    res.status(200).send("success");
  }catch(e) {
    console.error(e.message);
    res.sendStatus(200) && next(e);
  }
});

router.post('/uploadVideo', multipartMiddleware, function(req, res, next){
  console.log("inside function, outside try");
  try{
    console.log("inside server function", req.body, req.files);
    let file = req.files.file;
    var options = {
      args:
        [
          file.path
        ]
    };
    console.log('filename', file.path);
    PythonShell.run(`${__dirname}/MLModels/process_frame_0.py`, options, async function (err, data) {
      if (err) {
        console.log('error in python script---->', err)
        return res.status(500).send(err)
      }
      // let dataObj = JSON.parse(data)
      console.log('data received from python script', data)
      if(data[0] === 'True'){
        var folderName = file.path.split('/')[1];
        var targetFolder = PHOTO_FOLDER + folderName;
        var allFiles = [];
        fs.readdir(targetFolder, (err, files) => {
          files.forEach(file => {
            if(file !== 'full_frame.jpg'){
              console.log("target folder", targetFolder + '/' + file);
              var filePath = targetFolder + '/' + file;
              // allFiles.push({
              //   path: targetFolder + '/' + file,
              //   name: file
              // })
              uploadFile(filePath, file)
              allFiles.push({
                src: 'https://275-deepfake-images.s3-us-west-2.amazonaws.com/' + file,
                fileName: file
              })
            }
          });
          // console.log("filesssssss", allFiles);
          // res.zip(allFiles);
          const result = {
            files: allFiles,
            videoName: folderName
          }
          res.status(200).send(result);
        });
      }
      // res.status(200).send("success");
    })
  }catch(e) {
    console.error(e.message);
    res.sendStatus(400) && next(e);
  }
});

router.post('/getResults', function(req, res, next){
  console.log("from selectedPhoto", req.body);
  let {selectedPhotos, videoName} = req.body;
  const names = new Set();
  for(photo in selectedPhotos){
    names.add(selectedPhotos[photo].fileName);
    console.log(selectedPhotos[photo].fileName);
  }
  console.log(names);
  var uploadDir = `${__dirname}/../initial/${videoName}`;
  if (!fs.existsSync(uploadDir)){
    fs.mkdirSync(uploadDir);
  }
  var readDir = `${__dirname}/../initial/uploads/${videoName}`;
  var counter = 1;
  fs.readdir(readDir, function(err, filenames) {
    if (err) {
      console.log("read dir error", err);
      res.status(500).send("Internal server error");
    }
    filenames.forEach(function(filename) {
      if(names.has(filename)){
        console.log(filename);
        var oldPath = `${__dirname}/../initial/uploads/${videoName}/${filename}`;
        var newPath = `${__dirname}/../initial/${videoName}/${counter}.jpg`;
        fs.rename(oldPath, newPath, function (err) {
          if (err) {
            res.status(500).send("Internal server error");
          }
          console.log('Successfully renamed - AKA moved!')
        })
      }
    });
  });
  var options = {
    args:
      [
        './uploads/' + videoName,
        './initial/' + videoName
      ]
  };
  PythonShell.run(`${__dirname}/MLModels/process_one_video.py`, options, async function (err, data) {
    if (err) {
      console.log('error in python script---->', err)
      return res.status(500).send(err)
    }
    console.log('data received from initial script', data);
    var classifyOptions = {
      args: 
        [
  
        ]
    };
    PythonShell.run(`${__dirname}/MLModels/classify.py`, classifyOptions, async function (err, data) {
      if (err) {
        console.log('error in python script---->', err)
        return res.status(500).send(err)
      }
      console.log('data received from final script', data)
      res.status(200).send(data)
    })
  })
});

module.exports = router;
